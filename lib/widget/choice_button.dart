import 'package:flutter/material.dart';

class ChoiceButton extends StatelessWidget {
  final String text;
  final Color color;
  final Function() onPressed;

  const ChoiceButton({
    Key key,
    @required this.text,
    this.color = Colors.redAccent,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 4.0),
      child: RaisedButton(
        color: color,
        onPressed: onPressed,
        child: Text(
          text,
          textScaleFactor: 4.0,
        ),
      ),
    );
  }
}