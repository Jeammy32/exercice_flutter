import 'package:demo_flutter_ver_2/question/question_model.dart';
import 'package:demo_flutter_ver_2/widget/choice_button.dart';
import 'package:flutter/material.dart';

class QuestionRoute extends StatefulWidget {
  final List<QuestionModel> questions;

  const QuestionRoute({
    Key key,
    this.questions,
}) :super(key: key);

  @override
  State<StatefulWidget> createState() {
    return QuestionRouteState();
  }
}

class QuestionRouteState extends State<QuestionRoute> {
  int questionIndex = 0;

  @override
  Widget build(BuildContext context) {
    final question = widget.questions[questionIndex];

    return Scaffold(
      appBar: AppBar(
        title: Text(question.text),
      ),
      body: _buildBody(context,question),
    );
  }

  Widget _buildBody(BuildContext context, QuestionModel question) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: ChoiceButton(
            text: question.choice1,
            color: Colors.redAccent,
            onPressed: () {
              _onChoiceButtonPressed(context, 1);
            },
          ),
        ),
        Expanded(
          child: ChoiceButton(
            text: question.choice2,
            color: Colors.blueAccent,
            onPressed: () {
              _onChoiceButtonPressed(context, 2);
            },
          ),
        ),
      ],
    );
  }

  void _onChoiceButtonPressed(BuildContext context, int nbChoice) {
    setState(() {
      questionIndex = (questionIndex + 1) % widget.questions.length;
    });
  }
}