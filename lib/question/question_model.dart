class QuestionModel {
  final String text;
  final String choice1;
  final String choice2;
  final int nbChoice1;
  final int nbChoice2;

  QuestionModel(
    this.text,
    this.choice1,
    this.choice2,
    this.nbChoice1,
    this.nbChoice2,
  );
}
