import 'package:demo_flutter_ver_2/question/question_model.dart';
import 'package:demo_flutter_ver_2/question/question_route.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final question = [
      QuestionModel(
        "Would you rather...",
        "Have frog skin",
        "have chicken legs",
        10,
        20,
      ),
      QuestionModel(
        "Would you rather...",
        "Be as smart as Pierre",
        "Be as smart as Yvon",
        0,
        0,
      )
    ];

    return MaterialApp(
      title: 'Would you rather...',
      theme: ThemeData.dark(),
      home: QuestionRoute(
        questions: question,
      ),
    );
  }
}

